package cat.itb.dam.m07.geoguesser;

public class QuestionModel {
    int question;
    String answer;

    public QuestionModel(int question, String answer) {
        this.question = question;
        this.answer = answer;
    }

    public int getQuestion() {
        return question;
    }

    public void setQuestion(int question) {
        this.question = question;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }
}
