package cat.itb.dam.m07.geoguesser;

import androidx.lifecycle.ViewModel;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class QuestionViewModel extends ViewModel {

    List<QuestionModel> questions;
    int currentIndex = 0;
    double score = 0;
    int hints = 3;
    int progressBar = 10;

    int [] progressTexts = {R.string.numberQuestion1,R.string.numberQuestion2, R.string.numberQuestion3, R.string.numberQuestion4,
            R.string.numberQuestion5, R.string.numberQuestion6,  R.string.numberQuestion7, R.string.numberQuestion8, R.string.numberQuestion9, R.string.numberQuestion10};
    String [] answers = {"Madrid", "Paris", "Berlin", "Portugal", "Egipto", "Holanda" , "Japon", "Roma", "Moscu", "Pekin"};

    public QuestionViewModel(){
        questions = new ArrayList<>();
        questions.add(new QuestionModel(R.string.question1,"Madrid"));
        questions.add(new QuestionModel(R.string.question2,"Paris"));
        questions.add(new QuestionModel(R.string.question3,"Berlin"));
        questions.add(new QuestionModel(R.string.question4,"Portugal"));
        questions.add(new QuestionModel(R.string.question5,"Egipto"));
        questions.add(new QuestionModel(R.string.question6,"Holanda"));
        questions.add(new QuestionModel(R.string.question7,"Japon"));
        questions.add(new QuestionModel(R.string.question8,"Roma"));
        questions.add(new QuestionModel(R.string.question9,"Moscu"));
        questions.add(new QuestionModel(R.string.question10,"Pekin"));

        shuffle(questions);
    }

    private void shuffle (List<QuestionModel> questionsToShuffle){
        for (int i = 0; i < questionsToShuffle.size() -1 ; i++) {
            Random indexRandom = new Random();
            int index = indexRandom.nextInt(questionsToShuffle.size());
            QuestionModel questionAux = questionsToShuffle.get(index);
            questionsToShuffle.remove(index);
            questionsToShuffle.add(indexRandom.nextInt(questionsToShuffle.size()), questionAux);
        }
    }

    public int getCurrentQuestion (int index){
        return questions.get(index).getQuestion();
    }

    public String getCurrentAnswer (int index){
        return questions.get(index).getAnswer();
    }

    public void changeCurrentIndex(){
        currentIndex++;
        progressBar += 10;
    }

    private void resetIndex(){
        currentIndex = 0;
    }

}

