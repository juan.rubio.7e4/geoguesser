package cat.itb.dam.m07.geoguesser;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;


import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.Random;


public class MainActivity extends AppCompatActivity {

    TextView questionText;
    TextView numberOfQuestion;
    Button [] options = new Button[4];
    Button hint;
    TextView numberHints;
    ProgressBar progressBar;

    QuestionViewModel viewModel;
    AlertDialog.Builder builder;

    boolean hintQuestion = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        viewModel = new ViewModelProvider(this).get(QuestionViewModel.class);

        questionText = findViewById(R.id.questionText);
        numberOfQuestion = findViewById(R.id.questionNumber);
        options[0] = findViewById(R.id.button1);
        options[1] = findViewById(R.id.button2);
        options[2] = findViewById(R.id.button3);
        options[3] = findViewById(R.id.button4);
        hint = findViewById(R.id.buttonHint);
        numberHints = findViewById(R.id.textHint);
        progressBar = findViewById(R.id.progressBar);

        numberHints.setText(viewModel.hints+"");

        builder = new AlertDialog.Builder(this);
        builder.setTitle("Congratulations, you finished the quiz!");
        builder.setPositiveButton("Finish", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                System.exit(0);
            }
        });

        questionText.setText(viewModel.getCurrentQuestion(viewModel.currentIndex));
        numberOfQuestion.setText(viewModel.progressTexts[viewModel.currentIndex]);

        setButtons();
        allButtonsAreDiferent();
        progressBar.setProgress(viewModel.progressBar);

        options[0].setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkAnswer(0);
            }
        });

        options[1].setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkAnswer(1);
            }
        });

        options[2].setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkAnswer(2);
            }
        });

        options[3].setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkAnswer(3);
            }
        });

        hint.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hintQuestion = true;
                for (int i = 0; i < options.length ; i++) {
                    if (!options[i].getText().toString().equals(viewModel.getCurrentAnswer(viewModel.currentIndex))){
                        options[i].setVisibility(View.INVISIBLE);
                    }
                }
                viewModel.hints -= 1;
                numberHints.setText(viewModel.hints+"");
                if (viewModel.hints == 0){
                    hint.setVisibility(View.INVISIBLE);
                    numberHints.setVisibility(View.INVISIBLE);
                }
            }
        });

    }

    public void setButtons(){

        Random random = new Random();
        int randomButton = random.nextInt(3);
        int randomAnswer;

        for (int i = 0; i <viewModel.answers.length ; i++) {

            String answerQuestion = viewModel.getCurrentAnswer(viewModel.currentIndex);
            String answerButton = viewModel.answers[i];

            if (answerQuestion.equals(answerButton)){

                options[randomButton].setText(answerButton);
            }
        }

        for (int j = 0; j < options.length ; j++) {

            if (j != randomButton){

                randomAnswer = random.nextInt(viewModel.answers.length);
                options[j].setText(viewModel.answers[randomAnswer]);
            }
        }
    }

    public void allButtonsAreDiferent(){

        Random random = new Random();
        int randomAnswer;
        boolean diferent = false;

        while (!diferent){
            if (!options[0].getText().toString().equals( options[1].getText().toString())
                    && !options[0].getText().toString().equals(options[2].getText().toString())
                    && !options[0].getText().toString().equals(options[3].getText().toString())){
                if (!options[1].getText().toString().equals( options[0].getText().toString())
                        && !options[1].getText().toString().equals(options[2].getText().toString())
                        && !options[1].getText().toString().equals(options[3].getText().toString())){
                    if (!options[2].getText().toString().equals( options[0].getText().toString())
                            && !options[2].getText().toString().equals(options[1].getText().toString())
                            && !options[2].getText().toString().equals(options[3].getText().toString())){
                        if (!options[3].getText().toString().equals( options[0].getText().toString())
                                && !options[3].getText().toString().equals(options[1].getText().toString())
                                && !options[3].getText().toString().equals(options[2].getText().toString())){
                            diferent = true;
                        }else {
                            randomAnswer = random.nextInt(viewModel.answers.length);
                            options[3].setText(viewModel.answers[randomAnswer]);
                        }
                    }else {
                        randomAnswer = random.nextInt(viewModel.answers.length);
                        options[2].setText(viewModel.answers[randomAnswer]);
                    }

                }else {
                    randomAnswer = random.nextInt(viewModel.answers.length);
                    options[1].setText(viewModel.answers[randomAnswer]);
                }

            }else {
                randomAnswer = random.nextInt(viewModel.answers.length);
                options[0].setText(viewModel.answers[randomAnswer]);
            }

        }

    }

    public void changeQuestion(){
        viewModel.changeCurrentIndex();
        questionText.setText(viewModel.getCurrentQuestion(viewModel.currentIndex));
        numberOfQuestion.setText(viewModel.progressTexts[viewModel.currentIndex]);
        for (int i = 0; i <options.length ; i++) {
            options[i].setVisibility(View.VISIBLE);
        }
        setButtons();
        allButtonsAreDiferent();
        progressBar.setProgress(viewModel.progressBar);
    }

    public void checkAnswer(int index){
        if (options[index].getText().toString().equals(viewModel.getCurrentAnswer(viewModel.currentIndex))){
            if (!hintQuestion){
                viewModel.score++;
            }
            hintQuestion = false;
            if (viewModel.currentIndex!= 9){
                changeQuestion();
            }else{
                if (viewModel.score < 0 ){
                    viewModel.score = 0;
                }

                builder.setMessage("Your score is: " + (int)(viewModel.score*10) + "/100");
                builder.show();
            }

        }else {
            viewModel.score -= 0.5;
            if (viewModel.currentIndex!= 9){
                changeQuestion();
            }else{
                if (viewModel.score < 0 ){
                    viewModel.score = 0;
                }

                builder.setMessage("Your score is: " + (int)(viewModel.score*10) + "/100");
                builder.show();
            }
        }
    }
}